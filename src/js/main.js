const header = document.querySelector(".header");
const menu = document.querySelector(".mobile-menu");
const change = menu.children;
const itemName = document.querySelectorAll(".nav__link");
menu.addEventListener('click', openMenu);


function openMenu () {
    for( elem of change){
        elem.classList.toggle('active');
    }
    if(change[1].classList.value === "mobile-menu__close active"){
        const ul =document.createElement('ul');
        ul.classList.add('burger-menu');
        header.append(ul);
        for (const name of itemName) {
            const li =document.createElement('li');
            li.classList.add('burger-menu__item');
            ul.append(li);            
            const div =document.createElement('div');
            div.classList.add('burger-menu__item-hover');
            li.append(div);
            const p =document.createElement('p');
            p.classList.add('burger-menu__item-text');
            p.innerText = name.innerText;
            li.append(p);
        }
        const liempty =document.createElement('li');
        liempty.classList.add('burger-menu__item');
        liempty.classList.add('empty');
        ul.append(liempty);
    } 
    if(change[0].classList.value === "mobile-menu__open active"){
        const objectUl = document.querySelector(".burger-menu");
        objectUl.remove();
    } 
}

window.addEventListener('resize', () =>{
    if(document.body.clientWidth > 481){
        const objectUl = document.querySelector(".burger-menu");
        objectUl.remove();
        if(itemMenu[1].classList.value === "mobile-version__close active"){
            for( elem of itemMenu){
                elem.classList.toggle('active');
            }
        }    
    }
})